# SPDX-License-Identifier: GPL-3.0-or-later
"""
Worker class to build Vagrant images.
"""

import logging
import os
import subprocess

from .. import library
from .virtualbox_amd64 import VirtualBoxAmd64ImageBuilder

logger = logging.getLogger(__name__)


class VagrantImageBuilder(VirtualBoxAmd64ImageBuilder):
    """Image builder for Vagrant package."""
    include_contrib = True
    vagrant_extension = '.box'

    @classmethod
    def get_target_name(cls):
        """Return the name of the target for an image builder."""
        return 'vagrant'

    def build(self):
        """Run the image building process."""
        vm_file = self._replace_extension(self.image_file,
                                          self.vm_image_extension)
        vagrant_file = self._replace_extension(self.image_file,
                                               self.vagrant_extension)

        self.make_image()
        self.create_vm_file(self.image_file, vm_file)
        os.remove(self.image_file)
        self.vagrant_package(vm_file, vagrant_file)
        self.store_hash(vagrant_file)

    def vagrant_package(self, vm_file, vagrant_file):
        """Create a vagrant package from VM file."""
        command = [
            'bin/vagrant-package', '--distribution',
            self.arguments.distribution, '--release-components'
        ]
        command.extend(self.release_components)
        command += ['--output', vagrant_file, vm_file]
        library.run(command)

    def store_hash(self, vagrant_file):
        """Store the SHA-256 hash of the vagrant box file."""
        output = subprocess.check_output(['sha256sum', vagrant_file])
        result = output.decode()
        logger.info('sha256sum: %s', result)
        hash_filename = self._replace_extension(vagrant_file, '.sha256')
        with open(hash_filename, 'w') as hash_file:
            hash_file.write(result)
