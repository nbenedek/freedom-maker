# SPDX-License-Identifier: GPL-3.0-or-later
"""
Worker class to build i386 images.
"""

from .amd_intel import AMDIntelImageBuilder


class I386ImageBuilder(AMDIntelImageBuilder):
    """Image builder for all i386 targets."""
    architecture = 'i386'
    kernel_flavor = '686'
    include_non_free_firmware = True

    def __init__(self, *args, **kwargs):
        """Initialize builder object."""
        super().__init__(*args, **kwargs)
        self.packages += ['amd64-microcode', 'intel-microcode']
